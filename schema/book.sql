-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.5.7-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.3.0.5771
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for booksdb
CREATE DATABASE IF NOT EXISTS `booksdb` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `booksdb`;

-- Dumping structure for table booksdb.bookstore
CREATE TABLE IF NOT EXISTS `bookstore` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bookcover` varchar(255) DEFAULT NULL,
  `bookname` varchar(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `firstdate` varchar(255) DEFAULT NULL,
  `enddate` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- Dumping data for table booksdb.bookstore: ~2 rows (approximately)
/*!40000 ALTER TABLE `bookstore` DISABLE KEYS */;
INSERT INTO `bookstore` (`id`, `bookcover`, `bookname`, `author`, `firstdate`, `enddate`) VALUES
	(1, 'https://eloquentjavascript.net/img/cover.jpg', 'javaScript', 'john', '2020-12-03', '2020-12-03'),
	(18, 'https://images-na.ssl-images-amazon.com/images/I/81kqrwS1nNL.jpg', 'javaScript', 'tan', '2020-12-11', '2020-12-17');
/*!40000 ALTER TABLE `bookstore` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
