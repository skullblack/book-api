const express = require('express');
const app = express();
var cors = require('cors');
const port = 3006;
const mysql = require('mysql2');
const bodyParser = require('body-parser');

app.use(cors());
app.use(bodyParser.json());

const connection = mysql.createConnection({
  host: 'mariadb',
  user: 'root',
  password: '123456',
  database: 'booksdb',
  port: 3316,
});

app.get('/', (req, res) => {
  connection.query('SELECT * FROM `bookstore`', function (err, results) {
    if (!err) {
      res.send(results);
    } else {
      res.send(err);
    }
  });
});

app.post('/add', (req, res) => {
  connection.query(
    `INSERT INTO bookstore (bookcover,bookname,author,firstdate,enddate) VALUES (?,?,?,?,?)`,
    [
      req.body.data.bookcover,
      req.body.data.bookname,
      req.body.data.author,
      req.body.data.firstdate,
      req.body.data.enddate,
    ],
    function (err, results) {
      if (!err) {
        connection.query('SELECT * FROM `bookstore`', function (err, results) {
          if (!err) {
            res.send(results);
          } else {
            res.send(err);
          }
        });
      } else {
        res.send(err);
      }
    }
  );
});

app.post('/update', (req, res) => {
  connection.query(
    `UPDATE bookstore SET bookcover= ?, bookname= ?, author= ?, firstdate= ?, enddate= ? WHERE id= ? `,
    [
      req.body.data.bookcover,
      req.body.data.bookname,
      req.body.data.author,
      req.body.data.firstdate,
      req.body.data.enddate,
      req.body.data.id,
    ],
    function (err, results) {
      if (!err) {
        connection.query('SELECT * FROM `bookstore`', function (err, results) {
          if (!err) {
            res.send(results);
          } else {
            res.send(err);
          }
        });
      } else {
        res.send(err);
      }
    }
  );
});

app.delete('/delete/:id', (req, res) => {
  const {id} = req.params;
  connection.query(
    `DELETE FROM bookstore WHERE id=${id}`,

    function (err, results) {
      if (!err) {
        console.log('tannnn');
        connection.query('SELECT * FROM `bookstore`', function (err, results) {
          if (!err) {
            res.send(results);
          } else {
            res.send(err);
          }
        });
      } else {
        res.send(err);
      }
    }
  );
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
